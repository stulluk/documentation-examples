# Storage Budgeting Requirements (SBR) RUD

- [Storage Budgeting Requirements (SBR) RUD](#storage-budgeting-requirements-sbr-rud)
  - [Purpose](#purpose)
  - [Scope](#scope)
  - [Requirements](#requirements)
  - [ASRs & KDDs](#asrs--kdds)
  - [Flash Layout](#flash-layout)
  - [Partition Alignment](#partition-alignment)
  - [Storage Performance Tests for above ASRs](#storage-performance-tests-for-above-asrs)
  - [Storage Lifetime](#storage-lifetime)
  - [Theoritical eMMC lifetime expectancy](#theoritical-emmc-lifetime-expectancy)
    - [eMMC Lifetime Test](#emmc-lifetime-test)
    - [SystemCard Lifetime](#systemcard-lifetime)
    - [WAF Calculation](#waf-calculation)
    - [PowerOff Consistency](#poweroff-consistency)
    - [Further Reading](#further-reading)
    - [TODO](#todo)
    - [Open Issues](#open-issues)
  - [Change Log](#change-log)
  - [Authors](#authors)


## Purpose

1) Investigate/Measure the lifetime of flash devices(eMMC / SDcards...etc).
2) Measure the performance overhead of C2G storage layout.

## Scope

C2G devices uses multiple storage devices during operation, based on the use-cases and requirements:

| [![](images/SBR-Storages-v2.0.png)](images/SBR-Storages-v2.0.png)   | |
|---|---|

This document describes Storage Budgeting Requirements for the following storage mediums used in Comfort2G:

- eMMC : SM668 ( Internal Drive - 32Gbyte)
- Swissbit  SystemCard ( An SDHC card, used for autobackup - 32Gbyte)
- Panasonic SystemCard ( An SDXC card, used for autobackup - 32Gbyte)

Other storage mediums are not in the scope of this document.

## Requirements

> **REQ001**: Builtin and external storage devices used with C2G panels must survive 10 years of usage.

> **REQ002**: Lifetime of storage devices must be monitored during runtime.

## ASRs & KDDs

**ASR-001 Usage of LVM**: C2G Panels must use LVM partitioning in order to easily resizing partitions during OS update

**ASR-002 ReadOnly mount of Rootfs**: C2G Panels will readonly mount rootfs to avoid any corruption in the rootfs during operation.

**ASR-003 Usage of OverlayFS**: Main rootfs partition will be overlay mounted into data partition. By this way, we can easily remove overlay directory, and make factory default easily.

**ASR-004 Usage of RAID1**: Recovery partition, rootfs Partition and Datavolume partition will be mirrored with SystemCard via RAID1 technology. By this way, kernel / lvm software will perform bitwise syncing of storage mediums in the background.

> **Investigated alternatives**: rsync is not selected due to the fact that, growing number of files to be synced will cause logarithmic increase in the rsync duration and resource usage.

**ASR-005 Usage of ext4 filesystem**: All partitions (except APER and RecoveryOS) in the eMMC will use ext4 filesystem. Because according to our WAF and PowerOFF redundancy tests, ext4 is the "**best-fit-for-all**" (See WAF and PowerOFF redundancy tests below)

**ASR-006 Usage largest blocksize for OS update**: The larger the blocksze, the better the performance. ( See below BS tests)

**ASR-007 App level data consistency guarantee**: Every application on the system should carefully handle data consistency by properly implementing fflush() / fsync() / fclose() family of systemcalls. 

**ASR-008 OS level data consistency guarantee**: OS should guarantee 5secs of powerloss data consistency NFR by reducing dirty_expire and dirty_writeback timeout duration settings below 5 seconds.

## Flash Layout

According to above selected storage technologies, our flash layout is as belows:

[![](images/PartitionLayout2.png)](images/PartitionLayout2.png)


## Partition Alignment

Alignment should be at 1 megabyte boundary. current software RAID tools correctly align like this already.

For details, please see [Partition Alignment](03-SBR/PartitionAlignment)

## Storage Performance Tests for above ASRs

In order to perform below tests, we wrote following software: https://code.siemens.com/HMI_FW_TR/sbr/fioperf

---

> ### Effect of chosen blocksize on the R/W performance

| [![](images/blocksize/seqwrite-emmc-blocksize.png)](images/blocksize/seqwrite-emmc-blocksize.png)|[![](images/blocksize/seqread-emmc-blocksize.png)](images/blocksize/seqread-emmc-blocksize.png) |[![](images/blocksize/randread-emmc-blocksize.png)](images/blocksize/randread-emmc-blocksize.png) | [![](images/blocksize/randwrite-emmc-blocksize.png)](images/blocksize/randwrite-emmc-blocksize.png)
|---|---|---|---|

**Result**: The larger the blocksize, the better the performance, except for randwrite for large files.

---

> ### Sequential Read Performance Test comparison

| [![](images/comparisons/seqread/4KB_8GB.png)](images/comparisons/seqread/4KB_8GB.png)|[![](images/comparisons/seqread/128KB_8GB.png)](images/comparisons/seqread/128KB_8GB.png) |[![](images/comparisons/seqread/1MB_8GB.png)](images/comparisons/seqread/1MB_8GB.png) | [![](images/comparisons/seqread/128MB_8GB.png)](images/comparisons/seqread/128MB_8GB.png)
|---|---|---|---|

**Result**: Sequential read (such as **MANUAL BACKUP**) performance is not affected by the chosen technologies. The only effect for sequential read is the blocksize. The larger the blocksize, the better the performance.

---

> ### Sequential Write Performance Test comparison

| [![](images/comparisons/seqwrite/4KB_8GB.png)](images/comparisons/seqwrite/4KB_8GB.png)|[![](images/comparisons/seqwrite/128KB_8GB.png)](images/comparisons/seqwrite/128KB_8GB.png) |[![](images/comparisons/seqwrite/1MB_8GB.png)](images/comparisons/seqwrite/1MB_8GB.png) | [![](images/comparisons/seqwrite/128MB_8GB.png)](images/comparisons/seqwrite/128MB_8GB.png)
|---|---|---|---|

**Result**: Sequential write (Such as **OS-UPDATE**) performance is not affected by the chosen technologies. The only effect for sequential read is the blocksize. The larger the blocksize, the better the performance.

---

> ### Random Read Performance Test comparison

| [![](images/comparisons/randread/4KB_8GB.png)](images/comparisons/randread/4KB_8GB.png)|[![](images/comparisons/randread/128KB_8GB.png)](images/comparisons/randread/128KB_8GB.png) |[![](images/comparisons/randread/1MB_8GB.png)](images/comparisons/randread/1MB_8GB.png) | [![](images/comparisons/randread/128MB_8GB.png)](images/comparisons/randread/128MB_8GB.png)
|---|---|---|---|

**Result**: Random Read (Such as **OS-Normal operation, booting..etc**) performance is not affected by the chosen technologies. Because RAID1 choses fastest medium for random reads.

---

> ### Random Write Performance Test comparison

| [![](images/comparisons/randwrite/4KB_8GB.png)](images/comparisons/randwrite/4KB_8GB.png)|[![](images/comparisons/randwrite/128KB_8GB.png)](images/comparisons/randwrite/128KB_8GB.png) |[![](images/comparisons/randwrite/1MB_8GB.png)](images/comparisons/randwrite/1MB_8GB.png) | [![](images/comparisons/randwrite/128MB_8GB.png)](images/comparisons/randwrite/128MB_8GB.png)
|---|---|---|---|

**Result**: Random write (Such as **OS-Normal operation, debian package update**) performance is affected by RAID1 , because it limits overall performance to slowest medium for random writes. However, this is considered as acceptable, because there won't be a lot of writes during lifetime of the device.

## Storage Lifetime

## Theoritical eMMC lifetime expectancy

| [![](lifetime/Lifetime-expectancy-v1.0.png)](lifetime/Lifetime-expectancy-v1.0.png)   | |
|---|---|

### eMMC Lifetime Test

For testing lifetime change of of eMMC device , we created following software: https://code.siemens.com/sertac.tulluk/emmc-torture

Result of this test is as belows:


| [![](images/emmc-torture.png)](images/emmc-torture.png)   | |
|---|---|

> **NOTES**:
- Kernel does not update "life_time" file in sysfs, due to a design decision in MMC driver
- Lifetime of eMMC is reported in 269th byte of EXTCSD register
- This byte vary between 0x01 to 0x0b. 0x01 -> %100 , 0x0b -> end-of-life
- We used upstream mmc-utils package to retrieve 269th byte during runtime
- Our projection is, we can write 90-100TeraByte to our eMMC before end-of-life. 

### SystemCard Lifetime

For testing of Systemcards, we created following software: https://code.siemens.com/sertac.tulluk/sd-torture

Result of these tests are as follows:

|[![](sd-lifetime/SD-lifetime-x50.png)](sd-lifetime/SD-lifetime-x50.png)   | [![](sd-lifetime/SD-lifetime-x51.png)](sd-lifetime/SD-lifetime-x51.png)  |
|---|---|
| PANASONIC 32Gbyte SIMATIC SDCARD |  SWISSBIT 32Gbyte SIMATIC SDCARD | 

### WAF Calculation

|[![](images/waf-ratio.png)](images/waf-ratio.png) | |
|---|---|

> **NOTE**: WAF can not be calculated correctly, because above test is made by reading /proc/diskstats. We can not read from eMMc controller because it does not have TotalLBAsWritten interface...

### PowerOff Consistency

|[![](images/poweroff-consistency.png)](images/poweroff-consistency.png) | |
|---|---|

### Further Reading
SBR Performance Results C2G:
```
\\pnb04.siemens.net\PNB_HMI_FIRMWARE\20_Evaluation\Linux\05_System_Specification\RUD_SBR\01-Performance_AhmetcanSertac
```
### TODO
- [ ] 

### Open Issues

- [ ] How much Runtime writes to eMMC during operation ?
- [ ] How much EDGE apps writes to eMMC during operation ?

## Change Log
| Version   |   Date    |   Author  |   Status  |   Comment |
|:--|:--|:--|:--|:--|
| V2.00   |   22.01.2020    |   Sertac TULLUK  |   Done  | Add consistency ASRs |
| V1.00   |   22.01.2018    |   Sertac TULLUK  |   Done  |   Initial Draft |


## Authors
| Name 	| Role / Responsibility | Department  	| Loc. | Phone   |
|:----------------         |:----------------------|:------------------ |:-----|:--------|
| Tulluk, Sertac | Experienced R&D Engineer   | See SCD | IST  | See SCD |

