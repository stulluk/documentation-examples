# RUD - Passthrough Solution

## 1. General Information

C2G Panel has two card reader slots. One of them is connected to the CPU with SDIO pins, the other one uses the Genesys USB card reader chip.

![SdCardGeneralView](RUD-Passthrough/SdCardGeneralView.png "SdCardGeneralView")

C2G panel uses industrial grade SD cards to store data such as backups and logging during operation. 

To support industrial requirements (e.g. high temperature / humidity), we have specific “industrial grade” SD-cards from multiple vendors.

In order to identify the inserted SD card is an “industrial grade SD card” or commodity SD card, we must get CID information from SD card controller.

## 2. GenesysLogic SD Card Reader

GenesysLogic GL3233T is a USB card reader controller which supports data transfer between USB and SD card. It does not support transferring SD commands that define at SD specification document. So we wanted a custom firmware from Genesys, to read CID and other vendor specific information from the SD card. This firmware is stored in an external SPI flash chip connected to the GL3233 chip.

### 2.1. Firmware Update Functionality

To update firmware stored in the external SPI flash, we need the software update function.

GenesysLogic must provide a firmware update solution, which will update the firmware binary stored in SPI flash connected to the GL3233T IC over USB as shown in below drawing:

![FirmwareUpdate](RUD-Passthrough/FirmwareUpdate.PNG "Firmware Update")

#### ASRs & KDDs

- **ASR-001** This firmware update solution must allow updating the binary inside the SPI Flash at any time.

- **ASR-002** The solution must provide a way to get the status of the firmware:
    
    - Firmware update process successful or not (FW Update Status Check)
    - Current firmware working correctly or not (FW Status Check)

- **ASR-003** If a firmware update is unsuccessful and even GL3233T is powered off and on with bad firmware, a subsequent firmware update must be possible with this solution.

- **ASR-004** Firmware update solution must be able to performed from userspace.

- **ASR-004** Firmware update solution must not need any modification to kernel drivers (either built-in or external module)

### 2.2. Passthrough Solution

#### 2.2.1. CID Reading (CID Passthrough Solution)

GenesysLogic must provide a CID passthrough solution, which will be used to receive CID information from SD card controllers, and relay this information to USB host without any additional handling (transparent or plain passthrough). 

Basic data flow diagram shown in below:

![CidPassthroughSolution](RUD-Passthrough/CidPassthroughSolution.PNG "CID Passthrough Solution")

1. Host (Intel-Debian 9) will issue GetCID() command to GL3233T over USB
2. GL3233T will issue CMD10 (Send CID) to SD card
3. SD card will reply CID data to GL3233T
4. GL3233T will send this data to host

##### ASRs & KDDs

- **ASR-001** CID passthrough solution must work on all 5 SD cards;
    1. SwissBit SFSD032GL3BM1TO-I-HG-2CP-SIE (32Gbyte)
    2. Panasonic RP-SDNE32SJ0 (32Gbyte)
    3. Samsung MB-SD64D/EU (64Gbyte)
    4. Kingston Canvas React SDR/32GB (32Gbyte)
    5. Sandisk Industrial XI (32Gbyte)

- **ASR-002** CID passthrough solution must conform all timing and quality requirement defined in SD-SPEC (**[Standards, Specifications and Certifications section](#3-standards,-specifications-and-certifications)**).

- **ASR-003** CID passthrough solution to be provided by GenesysLogic must not affect or modify currently used USB-SD linux kernel drivers (GetCID() function must be called from linux userspace).

- **ASR-004** GenesysLogic must only provide an interface (documentation and example source code), which clearly explains how to send GetCID() command to their USB-to-SD card reader controller IC.

#### 2.2.2. Health Information Reading (Full Passthrough Solution)

GenesysLogic must provide a full passthrough solution, which will be used to send/receive SD commands to/from from SD card controllers, and relay this information to USB host. 

Basic data flow diagram shown in below:

![FullPassthroughSolution](RUD-Passthrough/FullPassthroughSolution.PNG "Full Passthrough Solution")

1. Host (Intel-Debian 9) will issue SendSDCommand(CMD-XX) command to GL3233T over USB
2. GL3233T will issue CMD-XX (SD Command) to SD card controller
3. SD card controller will reply CMD and DATA to GL3233T
4. GL3233T will send this data back to host over USB

##### ASRs & KDDs

- **ASR-001** The full passthrough solution must support all standard SD commands specified in SD-SPEC (CMD1 to CMD63 and ACMD1 to ACMD59). Because, for getting health information, for example, one SD card model may require sending SD-CMD56 + SD-ACMD13 + SD-CMD55 sequentially and reading reply back. However, another model may require SD-CMD16 + SD-ACMD6 + SD-CMD55. These commands, their arguments and sending order may differ from one SD card to other. Since we can not know all current and future SD card model behaviours for getting health information, we need to support all standard SD commands.

- **ASR-002** The full passthrough solution to be provided by GenesysLogic must not affect or modify currently used USB-SD linux kernel drivers (SendSDCommand() function must be called from userspace).

- **ASR-003** GenesysLogic must only provide an interface (documentation and example source code), which clearly explains how to send USB commands to their USB-to-SD card reader controller IC and get reply from it.

- **ASR-004** Full passthrough solution will be tested by Siemens for Swissbit and Panasonic SD cards.

- **ASR-005** Full passthrough solution must be “Plain Passthrough”. We don’t want GL3233T to evaluate any data. Only passthrough the commands and get their replies.

### 2.3. Quality Requirements

- **QREQ001:** Firmware update solution must work reliably and conform with USB-SPEC (**[Standards, Specifications and Certifications section](#3-standards,-specifications-and-certifications)**).

- **QREQ002:** CID and full passthrough solutions must conform all timing and quality requirements explained in SD-SPEC (**[Standards, Specifications and Certifications section](#3-standards,-specifications-and-certifications)**).

- **QREQ003:** CID and full passthrough solutions to be provided must work independent of:

    - **Call Frequency:** Host can issue getCID() or SendSDcommand() to GL3233T at reasonable frequency, specified in SD-SPEC Timings (Section 4.12).

## 3. Standards, Specifications and Certifications

- Universal Serial Bus 3.1 Specification Revision 1.0 July 26, 2013 must be fully supported

- SD Physical Layer Specification v6.00 April 10,2017 must be fully supported 

    - [Physical Layer Simplified Specification](https://www.sdcard.org/downloads/pls/)

